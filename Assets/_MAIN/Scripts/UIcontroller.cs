﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class UIcontroller : MonoBehaviour
{
    public TextMeshProUGUI metersIndicator;

    public GameObject pausePanel;

    public CharacterController characterController;

    public Slider powerSlider;

    public GameObject DeadPanel;

    public TextMeshProUGUI deadMeters;

    public GameObject rawImage, video;

    // Start is called before the first frame update
    void Start()
    {
        SetMeters();
        pausePanel.SetActive(false);
        DeadPanel.SetActive(false);
        StartCoroutine("WaitForVideo");
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            pausePanel.SetActive(!pausePanel.activeSelf);
        }
        SetMeters();
        powerSlider.value = (float)characterController.power / 100.0f;
    }

    public void SetMeters()
    {
        metersIndicator.text = string.Format("{0} mts", GameManager.Instance.meters);
        deadMeters.text = string.Format("{0} mts", GameManager.Instance.meters);
    }

    public void ActiveDeadPanel()
    {
        DeadPanel.SetActive(true);
    }

    public void ResetGame()
    {
        GameManager.Instance.LoadScene(1);
    }

    public void BackToMenu()
    {
        GameManager.Instance.LoadScene(0);
    }

    IEnumerator WaitForVideo()
    {
        yield return new WaitForSeconds(7.5f);

        rawImage.SetActive(false);
        video.SetActive(false);
    }
}
