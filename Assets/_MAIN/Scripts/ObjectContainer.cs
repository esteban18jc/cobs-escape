using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class ObjectContainer : MonoBehaviour
{
    #region Private properties

    [SerializeField, FoldoutGroup("Objects")]
    private List<GameObject> objects;

    #endregion

    #region Unity Functions

    #endregion

    #region Private Class Functions

    [Button("Load children")]
    private void LoadChildren()
    {
        foreach (Transform child in transform)
        {
            objects.Add(child.gameObject);
        }
        Debug.Log("Load all children in the objects list.");
    }

    [Button("Clean List")]
    private void CleanList()
    {
        objects.Clear();
        Debug.Log("Objects list cleaned.");
    }


    #endregion

    #region Public Class Functions

    public void HideAll()
    {
        foreach (GameObject obj in objects)
        {
            obj.SetActive(false);
        }
    }

    public void ShowOne(int index)
    {
        HideAll();
        objects[index].SetActive(true);
    }

    #endregion
}
