﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class SearchPlayer : MonoBehaviour
{
    void Start()
    {
        //InvokeRepeating("SearchAround", 6.0f, 1);
        SearchAround();
    }

    private void SearchAround()
    {
        StartCoroutine(WaithForSearch());
    }

    private void UpdateRotation(float value)
    {
        this.transform.SetPositionAndRotation(this.transform.position, Quaternion.Euler(this.transform.eulerAngles.x, this.transform.eulerAngles.y, value));
    }

    IEnumerator WaithForSearch()
    {
        yield return new WaitForSeconds(4);
        float newAngle = Random.Range(0.0f, 360.0f);
        float time = Random.Range(1.5f, 3.0f);
        Debug.Log(string.Format("from: {0} to {1} in time {2} sec.", this.transform.eulerAngles.z, newAngle, time));
        LeanTween.value(gameObject: gameObject, from: this.transform.eulerAngles.z, to: newAngle, time: time, callOnUpdate: UpdateRotation)
            .setOnComplete(SearchAround);
    }
}
