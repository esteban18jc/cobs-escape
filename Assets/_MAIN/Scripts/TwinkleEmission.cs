using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Sirenix.OdinInspector;

public class TwinkleEmission : MonoBehaviour // TODO Stop smoothly...
{
    #region Properties

    [Header("Twinkle Settings"), SerializeField]
    protected bool twinkleForever;

    [Space, SerializeField]
    protected int twinkleCycles;
    [SerializeField]
    protected float time;
    [SerializeField, ColorUsage(true, true, 0f, 8f, 0.125f, 3f)]
    protected Color correctColor;
    [SerializeField, ColorUsage(true, true, 0f, 8f, 0.125f, 3f)]
    protected Color wrongColor;

    [Space, SerializeField]
    protected LeanTweenType ease;

    [Header("Others"), SerializeField]
    protected List<Material> bannedMaterials = new List<Material>();

    [Space, SerializeField, FoldoutGroup("Unity Events")]
    protected UnityEvent onColorChangeStart;
    [SerializeField, FoldoutGroup("Unity Events")]
    protected UnityEvent onColorChangeEnd, onColorChangeUpdate;

    // Consts
    protected int NO_LOOP_INDEX = -1;

    // Events
    protected Action onColorChangeStartAction, onColorChangeEndAction, onColorChangeUpdateAction;

    // Cached Components
    protected Color initialColor;

    protected Material[] materials = null;

    // LeanTween
    private LTDescr twinkle;

    #endregion // Properties

    #region Unity functions

    private void Awake()
    {
        GetComponents();

        Suscribe();
    }

    private void Start()
    {
        Setup();
    }

    // ***

    private void OnEnable()
    {
        ResetEmission();
    }

    private void OnDisable()
    {
        ResetEmission();
    }

    // ***

    private void OnApplicationQuit()
    {
        ResetEmission();
    }

    #endregion // Unity functions

    #region Class functions

    protected void GetComponents()
    {

    }

    protected void Suscribe()
    {
        // ...
    }

    protected void Setup()
    {
        // ...
    }

    // ***

    protected void CancelTwinkleEmission()
    {
        //Log("TwinkleEmission :: CancelTwinkleEmission()");

        LeanTween.cancel(gameObject);

        ResetEmission();
    }

    protected void CancelTwinkleEmissionSmoothly()
    {
        //Log("TwinkleEmission :: CancelTwinkleEmissionSmoothly()");

        LeanTween.cancel(gameObject);

        ResetEmission(); // TODO Stop smoothly...
    }

    // ***

    public void StartCorrectTwinkle()
    {
        StopCorrectTwinkle();

        //Log("TwinkleEmission :: StartCorrectTwinkle()");

        Twinkle(correctColor);
    }

    public void StopCorrectTwinkle()
    {
        //("TwinkleEmission :: StopCorrectTwinkle()");

        CancelTwinkleEmission();
    }

    public void StopSmoothlyCorrectTwinkle()
    {
        //Log("TwinkleEmission :: StopSmoothlyCorrectTwinkle()");

        CancelTwinkleEmissionSmoothly(); // TODO Stop smoothly...
    }

    public void StartWrongTwinkle()
    {
        StopWrongTwinkle();

        //Log("TwinkleEmission :: StartWrongTwinkle()");

        Twinkle(wrongColor);
    }

    public void StopWrongTwinkle()
    {
        //Log("TwinkleEmission :: StopWrongTwinkle()");

        CancelTwinkleEmission();
    }

    public void StopSmoothlyWrongTwinkle()
    {
        //Log("TwinkleEmission :: StopSmoothlyWrongTwinkle()");

        CancelTwinkleEmissionSmoothly(); // TODO Stop smoothly...
    }

    // ***

    protected void Twinkle(Color feedbackColor)
    {
        TryGetMaterials();

        if (materials != null)
        {
            // Sets twinkle from color value...
            initialColor = materials.First().GetColor("_EmissionColor");

            if (twinkle != null)
                twinkle = null;

            // Start event...
            OnColorChangeStart();

            // Tween...
            twinkle = LeanTween.value(gameObject: gameObject, from: initialColor, to: feedbackColor, time: time, callOnUpdate: OnColorChangeUpdate)
                    .setEase(ease)
                    .setLoopPingPong((twinkleForever) ? NO_LOOP_INDEX : twinkleCycles);

            // End event...
            if (!twinkleForever)
                twinkle.setOnComplete(OnColorChangeEnd);
        }
    }

    // ***

    protected void OnColorChangeStart()
    {
       //Log("TwinkleEmission :: OnColorStart()");

        // Unity event...
        onColorChangeStart?.Invoke();

        // Action event...
        onColorChangeStartAction?.Invoke();
    }

    protected void OnColorChangeUpdate(Color value)
    {
        //Log(string.Format("TwinkleEmission :: OnColorUpdate() :: {0}", value));

        // Material color update main loop...
        if (materials != null)
        {
            if (materials.Count() > 0)
            {
                foreach (var material in materials)
                    material.SetColor("_EmissionColor", value);
            }
        }

        // Unity event...
        onColorChangeUpdate?.Invoke();

        // Action event...
        onColorChangeUpdateAction?.Invoke();
    }

    protected void OnColorChangeEnd()
    {
        //Log("TwinkleEmission :: OnColorEnd()");

        // Unity event...
        onColorChangeEnd?.Invoke();

        // Action event...
        onColorChangeEndAction?.Invoke();
    }

    // ***

    protected void TryGetMaterials()
    {
        // Get materials from child's
        materials = GetComponentsInChildren<MeshRenderer>()
            .Select(renderer => renderer.material)
            .Where(material => !bannedMaterials.Contains(material))
            .Distinct()
            .ToArray(); // TODO Support 'materials' instead of 'material'
    }

    // ***

    protected void ResetEmission()
    {
        // Reset material color emission...
        if (materials != null)
        {
            foreach (var material in materials)
                material.SetColor("_EmissionColor", (initialColor != null) ? initialColor : Color.clear);
        }
    }

    #endregion // Class functions
}
