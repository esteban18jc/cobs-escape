﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    
    public void LoadGame()
    {
        GameManager.Instance.LoadScene(1);
    }

    public void EndGame()
    {
        Application.Quit();
    }
}
