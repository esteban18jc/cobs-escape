﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{

    public List<AudioSource> musicList;
    public List<AudioSource> soundsList;

    // Singleton!
    public static AudioManager Instance
    {
        get; private set;
    }

    private void Awake()
    {
        if (Instance != null)
        {
            DestroyImmediate(this);
            return;
        }
        else
            Instance = this;


        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        HideSounds();
        PlayMusic();
    }

    private void HideSounds()
    {
        for (int i = 0; i < soundsList.Count; i++)
        {
            soundsList[i].gameObject.SetActive(false);
        }
    }
    public void PlaySound(int i)
    {
        HideSounds();
        soundsList[i].gameObject.SetActive(true);
    }

    public void PlayMusic()
    {
        for (int i = 0; i < musicList.Count; i++)
        {
            musicList[i].gameObject.SetActive(true);
        }
    }
}
