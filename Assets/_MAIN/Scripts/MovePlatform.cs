﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlatform : MonoBehaviour
{
    public Axis axis = Axis.X;

    public Vector3 startPos, endPos;
    public float time;

    public bool isLooping = true;

    public int loopCycles = 1;

    private void Start()
    {
        StartMovement();
    }

    public void StartMovement()
    {
        switch (axis)
        {
            case Axis.X:
                LeanTween.value(gameObject: gameObject, from: startPos.x, to: endPos.x, time: time, callOnUpdate: updatePosition).setLoopPingPong(isLooping ? -1 : loopCycles);
                break;
            case Axis.Y:
                LeanTween.value(gameObject: gameObject, from: startPos.y, to: endPos.y, time: time, callOnUpdate: updatePosition).setLoopPingPong(isLooping ? -1 : loopCycles);
                break;
            case Axis.Z:
                LeanTween.value(gameObject: gameObject, from: startPos.z, to: endPos.z, time: time, callOnUpdate: updatePosition).setLoopPingPong(isLooping ? -1 : loopCycles);
                break;
            default:
                break;
        }
    }

    private void updatePosition(float value)
    {
        switch (axis)
        {
            case Axis.X:
                this.transform.LeanSetLocalPosX(value);
                break;
            case Axis.Y:
                this.transform.LeanSetLocalPosY(value);
                break;
            case Axis.Z:
                this.transform.LeanSetLocalPosZ(value);
                break;
            default:
                break;
        }
    }

    [Button("Set start position")]
    public void SetStartPosition()
    {
        startPos = transform.localPosition;
        Debug.Log("Start position was seted as " + startPos);
    }

    [Button("Set end position")]
    public void SetEndPosition()
    {
        endPos = transform.localPosition;
        Debug.Log("End position was seted as " + endPos);
    }
}

public enum Axis
{
    X,Y,Z
}