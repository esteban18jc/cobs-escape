﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;
using UnityEngine.Events;
using Cinemachine;
using System;

public class CharacterController : MonoBehaviour
{

    public float frodwardThrust = 1.0f;
    public Rigidbody2D rb;
    private Animator m_animator;
    public float jumpThrust = 1.0f;
    public float maxVelocity;

    public float rayOffset;
    public float rayLenght;

    public Light2D m_light;

    private bool isAlive;

    public UnityEvent OnDead, OnJump;

    public CinemachineVirtualCamera cm_cam;

    public int power;
    public float maxPowerTime;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Damage")
        {
            KillMe();
        }

        if (collision.collider.name == "Plataforma_Movil")
        {
            this.transform.SetParent(collision.collider.transform);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.name == "Plataforma_Movil")
        {
            this.transform.SetParent(null);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Damage")
        {
            KillMe();
        }
        if (collision.transform.tag == "Camera")
        {
            CatchMe();
        }

        if (collision.transform.tag == "Point")
        {
            AddPower(5);
            collision.gameObject.SetActive(false);
            AudioManager.Instance.PlaySound(2);
        }
    }

    public void CatchMe()
    {
        if (isAlive)
        {
            m_animator.SetTrigger("Surprise");
            Invoke("KillMe", 1.5f);
        }
    }

    public void KillMe()
    {
        if (isAlive)
        {
            isAlive = false;
            m_animator.SetTrigger("Dead");
            LeanTween.value(gameObject: gameObject, from: m_light.pointLightOuterRadius, to: 1.5f, time: 1.2f, callOnUpdate: updateLight);
            cm_cam.Follow = null;
            AudioManager.Instance.PlaySound(0);
            OnDead.Invoke();
        }
    }

    private void updateLight(float value)
    {
        m_light.pointLightOuterRadius = value;
    }

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        m_animator = GetComponent<Animator>();
        isAlive = true;
    }

    public void AddPower(int amount)
    {
        if (power + amount <= 100)
            power += amount;
    }

    void FixedUpdate()
    {
        m_animator.SetFloat("VerticalVel", rb.velocity.y);

        if (isAlive)
        {
            RaycastHit2D hit = Physics2D.Raycast(transform.position + (Vector3.up * rayOffset), -Vector2.one, rayLenght);
            RaycastHit2D hit2 = Physics2D.Raycast(transform.position + (Vector3.up * rayOffset), new Vector2(1.0f, -1.0f), rayLenght);
            Debug.DrawRay(transform.position + (Vector3.up * rayOffset), -Vector2.one * rayLenght, Color.red);
            Debug.DrawRay(transform.position + (Vector3.up * rayOffset), new Vector2(1.0f, -1.0f) * rayLenght, Color.red);
            GameManager.Instance.meters = (int)this.transform.position.x;
            if (hit.collider != null || hit2.collider != null)
            {
                if (hit.collider?.tag == "Floor" || hit2.collider?.tag == "Floor")
                {
                    if (Input.GetKey(KeyCode.Space))
                    {
                        rb.AddForce(Vector3.up * jumpThrust);
                        if (rb.velocity.x <= maxVelocity)
                            rb.AddForce(Vector3.right * frodwardThrust);
                        AudioManager.Instance.PlaySound(1);
                        OnJump.Invoke();
                    }
                    if (Input.GetKeyDown(KeyCode.Space))
                    {
                        AudioManager.Instance.PlaySound(1);
                    }
                }
            }

            if (Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift))
            {
                if(power > 0)
                    DoPower();
            }
        }

    }

    private void DoPower()
    {
        LeanTween.value(gameObject: gameObject, from: m_light.pointLightOuterRadius, to: 10.6f, time: 0.8f, callOnUpdate: updateLight).setOnComplete(TurnOffPower);
    }

    private void TurnOffPower()
    {
        float time = (power * maxPowerTime) / 100;
        LeanTween.value(gameObject: gameObject, from: power, to: 0.0f, time: time, callOnUpdate: updatePower);
        LeanTween.value(gameObject: gameObject, from: m_light.pointLightOuterRadius, to: 5.5f, time: time, callOnUpdate: updateLight);
    }

    private void updatePower(float value)
    {
        power = (int)value;
    }

}
