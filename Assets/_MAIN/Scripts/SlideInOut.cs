///-----------------------------------------------------------------
///   Author : Esteban Jim�nez Calder�n                    
///   Date   : 07/01/2020 
///   Company: NEDIAR
///-----------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Sirenix.OdinInspector;
using UnityEngine.Events;
using UnityEditor;

public class SlideInOut : MonoBehaviour
{
    #region Properties

    [Header("Curve for animation"), Space, SerializeField]
    private LeanTweenType fadeOutCurve;

    [Header("Animation time"), Space, SerializeField]
    private float animationTime;

    [Header("Positional vectors"), Space, SerializeField]
    private Vector3 startPosition, endPosition;

    [Space]
    public UnityEvent onSlideOutEnd, onSlideInEnd;

    #endregion

    #region Unity Methods

    private void Start()
    {
        Setup();
    }

    #endregion

    #region Private Class Methods

    private void Setup()
    {
        if(startPosition != null)
            transform.localPosition = startPosition;

        print(startPosition + " and "  + transform.localPosition);
    }

    private bool CheckPositionVectors()
    {
        if (endPosition == null || startPosition == null)
        {
            Debug.LogError("You do not set the position(s) to do the slide animation.");
            return false;
        }

        return true;
    }

    #endregion

    #region Public Class Methods

    public void SetStartPosition()
    {
        startPosition = transform.localPosition;
        Debug.Log("Start position was seted as " + startPosition);
    }

    public void SetEndPosition()
    {
        endPosition = transform.localPosition;
        Debug.Log("End position was seted as " + endPosition);
    }

    public void SlideOut()
    {
        if (!CheckPositionVectors() || transform.localPosition == endPosition)
            return;

        LeanTween.cancel(this.gameObject);

        LeanTween.moveLocal(this.gameObject, endPosition, animationTime).setEase(fadeOutCurve).setOnComplete(() => {
            //Do something when the tween ended
            Debug.Log("Slide Out finished");
            onSlideOutEnd.Invoke();
        });
    }

    public void SlideIn()
    {
        if (!CheckPositionVectors() || transform.localPosition == startPosition)
            return;

        LeanTween.cancel(this.gameObject);

        LeanTween.moveLocal(this.gameObject, startPosition, animationTime).setEase(fadeOutCurve).setOnComplete(() => {
            //Do something when the tween ended
            Debug.Log("Slide Out finished");
            onSlideInEnd.Invoke();
        });
    }

    #endregion
}

#if UNITY_EDITOR
[CustomEditor(typeof(SlideInOut))]
public class customInspector : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        SlideInOut slideInOut = (SlideInOut)target;

        if (GUILayout.Button("Set start position"))
        {
            slideInOut.SetStartPosition();
        }

        if (GUILayout.Button("Set end position"))
        {
            slideInOut.SetEndPosition();
        }
    }
}
#endif
