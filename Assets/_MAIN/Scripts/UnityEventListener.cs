///-----------------------------------------------------------------
///   Author : Juan David Jaramillo                    
///   Date   : 27/12/2018 09:32
///   Company: NEDIAR
///-----------------------------------------------------------------

using UnityEngine.Events;

namespace UnityEngine
{
    namespace Utils
    {
        public class UnityEventListener : MonoBehaviour
        {
            #region Properties

            [Space, SerializeField]
            private UnityEvent onEnable, onDisable;

            #endregion

            #region Unity functions

            private void OnEnable()
            {
                if (onEnable != null)
                    onEnable.Invoke();
            }

            private void OnDisable()
            {
                if (onDisable != null)
                    onDisable.Invoke();
            }

            #endregion
        }
    }
}
