﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{

    public int meters;

    // Singleton!
    public static GameManager Instance
    {
        get; private set;
    }

    private void Awake()
    {
        if (Instance != null)
        {
            DestroyImmediate(this);
            return;
        }
        else
            Instance = this;


        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        ResetMeters();
    }

    public void ResetMeters()
    {
        meters = 0;
    }

    public void LoadScene(int sceneIndex)
    {
        SceneManager.LoadSceneAsync(sceneIndex);
    }

    


}
