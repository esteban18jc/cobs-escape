﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class MapGenerator : MonoBehaviour
{
    #region Properties

    [SerializeField, TitleGroup("Map Settings")]
    private int poolFactorPerPrefab;

    [SerializeField, TitleGroup("Map Settings")]
    private int initialMapSize;

    [SerializeField, TitleGroup("Map Settings"), Tooltip("Collider for check the player")]
    private Collider2D playerChecker;

    [SerializeField, TitleGroup("Map Settings")]
    private int checkerOffset = 5;

    private int checkerCounter = 1;

    [SerializeField, TitleGroup("Block Settings")]
    private int blockSize;

    [SerializeField, TitleGroup("Block Settings")]
    private int currrentBlocks = 0;

    [SerializeField, ReadOnly, TitleGroup("Block Settings")]
    private List<GameObject> blocksPool;
    [SerializeField, ReadOnly, TitleGroup("Block Settings")]
    private List<GameObject> blocksInGame = new List<GameObject>();

    [SerializeField, TitleGroup("Prefab Floor blocks")]
    private List<GameObject> blocks;

    #endregion

    #region Unity Functions

    private void Awake()
    {
        if (!CheckFloorBlocks())
            Debug.LogError("You must add at least one prefab.");
    }

    private void Start()
    {
        StartPool();
        GenerateMap();
        SetCheckerPosition();
    }

    #endregion

    #region Class Functions

    // PULBIC FUNCTIONS
    
    public void CreateBlock()
    {
        InstantiateBlock(new Vector3(currrentBlocks * blockSize, 0.0f, 0.0f), Quaternion.identity);
    }

    public void DestroyFirstBlock()
    {
        GameObject oldObj = blocksInGame[0];
        oldObj.SetActive(false);
        blocksPool.Add(oldObj);
        blocksInGame.Remove(oldObj);
    }

    public void MoveCheckerPosition()
    {
        playerChecker.transform.SetPositionAndRotation(new Vector3(playerChecker.transform.position.x + blockSize, 0.0f, 0.0f), Quaternion.identity);
    }

    // PRIVATE FUNCTIONS

    private void SetCheckerPosition()
    {
        playerChecker.transform.SetPositionAndRotation(new Vector3(checkerOffset * checkerCounter * blockSize, 0.0f, 0.0f), Quaternion.identity);
        checkerCounter++;
    }

    private GameObject InstantiateBlock(Vector3 newPosition, Quaternion newRotation)
    {
        GameObject newObj = blocksPool[Random.Range(0, blocksPool.Count - 1)];
        blocksInGame.Add(newObj);
        blocksPool.Remove(newObj);
        newObj.SetActive(true);
        newObj.transform.SetPositionAndRotation(newPosition, newRotation);
        currrentBlocks++;
        return newObj;
    }

    private bool CheckFloorBlocks()
    {
        return blocks.Count > 0;
    }

    private void StartPool()
    {
        blocksPool = new List<GameObject>();

        for (int i = 0; i < poolFactorPerPrefab; i++) 
        {
            for (int j = 0; j < blocks.Count; j++)
            {
                GameObject newObj = Instantiate(blocks[j]);
                blocksPool.Add(newObj);
                newObj.SetActive(false);
            }
        }
    }

    private void GenerateMap()
    {
        for (int i = 0; i < initialMapSize; i++)
        {
            CreateBlock();
        }
    }

    #endregion
}
