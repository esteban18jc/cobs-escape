﻿using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class CollisionEventsListener : MonoBehaviour
{
    #region Private Properties

    [SerializeField, Space]
    private bool showMessages;

    [SerializeField, FoldoutGroup("Collision Settings"), Space]
    private CollisionType collisionType;

    [SerializeField, FoldoutGroup("Collision Settings"), Space, Tooltip("If the length is equal to 0 all collisions are gonna be valid.")]
    private List<string> validTags = new List<string>();

    [SerializeField, FoldoutGroup("Collision Events"), Space]
    private UnityEngine.Events.UnityEvent onCollisionEnter, onCollisionExit, onCollisionStay;

    [SerializeField, FoldoutGroup("Trigger Events"), Space]
    private UnityEngine.Events.UnityEvent onTriggerEnter, onTriggerExit, onTriggerStay;

    #endregion

    #region Collision Functions

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Activation");

        if (!CheckCollisionType(CollisionType.Collision) || !CheckTags(collision.collider.tag))
            return;

        onCollisionEnter?.Invoke();

        if (showMessages)
            Debug.Log(string.Format("Collision Event Active :: On Collision ENTER. Collision between {0} (Me) and {1} (Other)", this.gameObject.name, collision.collider.gameObject.name));
    }

    private void OnCollisionExit(Collision collision)
    {
        Debug.Log("Activation");

        if (!CheckCollisionType(CollisionType.Collision) || !CheckTags(collision.collider.tag))
            return;

        onCollisionExit?.Invoke();

        if (showMessages)
            Debug.Log(string.Format("Collision Event Active :: On Collision EXIT. Collision between {0} (Me) and {1} (Other)", this.gameObject.name, collision.collider.gameObject.name));
    }

    private void OnCollisionStay(Collision collision)
    {
        Debug.Log("Activation");

        if (!CheckCollisionType(CollisionType.Collision) || !CheckTags(collision.collider.tag))
            return;

        onCollisionStay?.Invoke();

        if (showMessages)
            Debug.Log(string.Format("Collision Event Active :: On Collision STAY. Collision between {0} (Me) and {1} (Other)", this.gameObject.name, collision.collider.gameObject.name));

        // Debug-draw all contact points and normals
        foreach (ContactPoint contact in collision.contacts)
        {
            Debug.DrawRay(contact.point, contact.normal * 10, Color.white);
        }
    }

    #endregion

    #region Trigger Functions

    private void OnTriggerEnter(Collider collider)
    {
        if (!CheckCollisionType(CollisionType.Trigger) || !CheckTags(collider.tag))
            return;

        onTriggerEnter?.Invoke();

        if (showMessages)
            Debug.Log(string.Format("Collision Event Active :: On Trigger ENTER. Collision between {0} (Me) and {1} (Other)", this.gameObject.name, collider.gameObject.name));
    }

    private void OnTriggerExit(Collider collider)
    {
        if (!CheckCollisionType(CollisionType.Trigger) || !CheckTags(collider.tag))
            return;

        onTriggerExit?.Invoke();

        if (showMessages)
            Debug.Log(string.Format("Collision Event Active :: On Trigger EXIT. Collision between {0} (Me) and {1} (Other)", this.gameObject.name, collider.gameObject.name));
    }

    private void OnTriggerStay(Collider collider)
    {
        if (!CheckCollisionType(CollisionType.Trigger) || !CheckTags(collider.tag))
            return;

        onTriggerStay?.Invoke();

        if (showMessages)
            Debug.Log(string.Format("Collision Event Active :: On Trigger STAY. Collision between {0} (Me) and {1} (Other)", this.gameObject.name, collider.gameObject.name));
    }

    #endregion

    #region Class functions

    private bool CheckCollisionType(CollisionType sourceType)
    {
        return sourceType == collisionType || collisionType == CollisionType.Both;
    }

    private bool CheckTags(string sourceTag)
    {
        if (validTags.Count == 0)
            return true;

        return validTags.Contains(sourceTag);
    }

    #endregion
}

public enum CollisionType
{
    Collision,
    Trigger,
    Both
}
