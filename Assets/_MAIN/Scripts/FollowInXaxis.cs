﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowInXaxis : MonoBehaviour
{
    public Transform target;

    private void FixedUpdate()
    {
        transform.position = new Vector3(target.position.x, transform.position.y, transform.position.z);
    }
}
